# sobi

**S**amir's **O**wn **B**lockchain **I**mplentation

maybe "sorbi" later? (<u>so</u> <u>r</u>udimentary/<u>r</u>ough <u>bi</u>)<br>
i like sobi better,,

Inspired by https://github.com/Icesofty/blockchain-demo and [this video by 3Blue1Brown](https://www.youtube.com/watch?v=bBC-nXj3Ng4)

View the trello here: https://trello.com/b/nxss6xNu/sobi-todo

### ⚠️ I am aware I might be doing it wrong.
(most of the time.)

i am *learning* as i go along. please be patient.<br>
if you have any tips, lmk via the issues <3

### Things I am learning:
* TypeScript! I'm writing the entire project in this language. So far it's pretty neat :)
  * Types/interfaces
  * Access modifiers
  * Classes
* [Jest](https://jestjs.io/) testing framework
* Strict code style enforcement with eslint and typescript-eslint<br>(This is something I was already doing, but I've really taken it to the next level with this project.)

And on a higher/more abstract level
* Object-oriented programming- something that now that I've learned, I can't go back. Javascript feels very loosey-goosey now.
* Type-strict languages- technically typescript doesn't have to be type-strict but I've set the compiler to be strict to take full advantage of the language's features.
* Properly testing code- in this case using the [jest](https://jestjs.io) testing framework. Thanks fireship.io for [teaching me about it](https://www.youtube.com/watch?v=Jv2uxzhPFl4).

## Start it up 🚀
> This project is *far* from ready. The server doesn't even work yet. However, if you copy [this specific file at this specific commit](https://gitlab.com/akii0008/sobi/-/blob/5bbdb316bea2aeb61349b05ea377c539533b9799/src/old-gamut.ts) into the `src/` folder root, build it, then run it, it'll test the classes in the `common/` folder.<br>

> You'll need node.js version 16 or higher to run this project.<br>
> I'll also be using the yarn package manager instead of npm- the commands are very similar, I'll edit this later to include npm.

0. Run `yarn` to install the necessary dependencies.
1. Run `yarn build` to transpile the project to JavaScript
2. From here you can do one of a few things:
  * Run the project in its entirety by running `yarn start`, which will boot both the client and the server, where the client connects to the local server. This is not ideal for a production setup- you'll be talking to yourself instead of other users on the network, but it's what I do for development.
  * Run `yarn start-client` which will start *just* the client. By default, it will try and connect to a local server (which won't exist if you're using this method), so you'll have to define a SOBI server IP via an environment variable. **Do not include the `ws://` or port.** These will be automatically included.
  * Run `yarn start-server` which will start *just* the server. As of writing this, the server doesn't actually do anything yet. Clients can connect, but there is no functionality. That'll come soon.

## [CHANGELOG.md](CHANGELOG.md)

## [LICENSE.md](LICENSE.md)
[![GPLv3](https://i.imgur.com/B76AJTI.png)](https://choosealicense.com/licenses/gpl-3.0)
