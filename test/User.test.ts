/* eslint-disable @typescript-eslint/ban-ts-comment */

import User from "../src/common/classes/User";
import Transaction from "../src/common/classes/Transaction";
import createUser from "../src/common/util/createUser";
import { rmdir, access } from "fs/promises";
import { constants } from "fs";
import * as openpgp from "openpgp";

describe("User class testing", () => {

  jest.setTimeout(10000);

  expect.extend({
    toStartWith(received: string, string: string) {
      const pass = received.startsWith(string);

      if (pass) return {
        message: () => `expected ${received} to not start with string "${string}"`,
        pass: true
      };
      else return {
        message: () => `expected ${received} to start with string "${string}"`,
        pass: false
      };
    }
  });

  let user: User;

  // Run before the test suite begins
  beforeAll(async () => {
    // as a bonus, this function gets tested too :)
    // This also covers the two static functions User#createKeyFiles and User#generatePair.
    user = await createUser({ username: "akii0008", email: "e@mail.com", passphrase: "very-cool-passphrase" });

    // wait a spell for the files to generate
    return await new Promise((r) => setTimeout(r, 1000));
  });

  // Run before every single test
  /* beforeEach(async () => {
    // nothing here yet. i dont know if i'll need this
  }); */

  test("Address should be public", () => {
    // @ts-ignore 2339
    return expect(user.address).toStartWith("sobi-");
  });

  test("Username should be public", () => {
    return expect(user.username).toBeTruthy();
  });

  test("Email should be public", () => {
    return expect(user.email).toBeTruthy();
  });

  test("Private key should be private", () => {
    // @ts-ignore 2339
    return expect(user.privateKey).toBeUndefined();
  });

  test("Public key should be public", () => {
    return expect(user.publicKey).toBeTruthy();
  });

  test("Revocation cert should be private", () => {
    // @ts-ignore 2339
    return expect(user.revocationCertificate).toBeUndefined();
  });

  test("Public key file should exist", async () => {
    return expect(access(`./data/users/${user.address}/public.key`, constants.F_OK)).resolves.not.toThrow();
  });

  test("Private key file should exist", async () => {
    return expect(access(`./data/users/${user.address}/private.key`, constants.F_OK)).resolves.not.toThrow();
  });

  test("Revocation cert file should exist", async () => {
    return expect(access(`./data/users/${user.address}/revoke.crt`, constants.F_OK)).resolves.not.toThrow();
  });

  test("User#toString() should return a string", () => {
    return expect(typeof user.toString()).toBe("string");
  });

  test("User#getPrivateKey should return an Promise<openpgp#PrivateKey> object", () => {
    return expect(user.getPrivateKey("very-cool-passphrase")).resolves.toBeInstanceOf(openpgp.PrivateKey);
  });

  test("User#send should return a Promise<Transaction> object", async () => {
    return expect(user.send("sobi-2Uw1Nbv8Q1FLV", 100, 0, "very-cool-passphrase")).resolves.toBeInstanceOf(Transaction);
  });

  test("User#send should not be able to send to themself", async () => {
    return expect(user.send(user.address, 100, 0, "very-cool-passphrase")).rejects.toThrow("You cannot send SOBI to yourself");
  });

  // Runs after the entire test suite has finished
  afterAll(async () => {
    return await rmdir(`./data/users/${user.address}`, { recursive: true });
  });
});