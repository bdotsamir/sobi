import * as openpgp from "openpgp";
import { access, constants, mkdir, writeFile, chmod } from "fs";
import bs58 from "bs58check";
import logger from "../util/logger";
import createTransaction from "../util/createTransaction";
import Transaction from "./Transaction";
import Address from "../interfaces/Address";

interface IGeneratePair {
  username: string,
  email: string,
  passphrase: string
}

interface GeneratedPair {
  address: Address,
  privateKey: string,
  publicKey: string,
  revocationCertificate: string
}

interface IUser {
  address: Address,
  username: string,
  email: string,
  privateKey: string,
  publicKey: string,
  revocationCertificate: string,
  dontGenerateFiles?: boolean
}

export default class User {

  public readonly address: Address;
  public readonly username: string;
  public readonly email: string;
  readonly #privateKey: string; // the # means it's a private property according to js
  public readonly publicKey: string;
  readonly #revocationCertificate: string;

  constructor({ address, username, email, privateKey, publicKey, revocationCertificate, dontGenerateFiles }: IUser) {
    this.address = address;
    this.username = username;
    this.email = email;
    this.#privateKey = privateKey;
    this.publicKey = publicKey;
    this.#revocationCertificate = revocationCertificate;

    if (dontGenerateFiles) return;
    // Check if the user path exists, if not, make it.
    const userPath = `./data/users/${this.address}`;

    // at this point, userPath will definitely exist.
    access(userPath, constants.F_OK, async accessError => {
      if (!accessError) logger.log(`Dir ${userPath} exists. Key files have already been created in a previous iteration.`);
      else { // user path does not exist. create one.
        mkdir(userPath, { recursive: true }, mkdirError => {
          if (mkdirError) throw mkdirError;
          logger.verbose(`Dir ${userPath} does not exist. Making it now`);

          User.createKeyFiles(userPath, privateKey, publicKey, revocationCertificate);
        });
      }
    });
  }

  public toString(): string {
    return `${this.username} <${this.email}> - ${this.address}`;
  }

  private static createKeyFiles(userPath: string, sK: string, pK: string, rC: string): void {
    writeFile(`${userPath}/private.key`, sK, err => {
      if (err) throw err;
      logger.verbose(`Wrote private key to ${userPath}/private.key`);

      chmod(`${userPath}/private.key`, constants.S_IRUSR | constants.S_IWUSR, chmodErr => {
        if (chmodErr) throw chmodErr;
        logger.verbose(`Changed permissions to -rw------- for ${userPath}/private.key`);
      });
    });

    writeFile(`${userPath}/public.key`, pK, err => {
      if (err) throw err;
      logger.verbose(`Wrote public key to ${userPath}/public.key`);

      chmod(`${userPath}/public.key`, constants.S_IRUSR | constants.S_IWUSR, chmodErr => {
        if (chmodErr) throw chmodErr;
        logger.verbose(`Changed permissions to -rw------- for ${userPath}/public.key`);
      });
    });

    writeFile(`${userPath}/revoke.crt`, rC, err => {
      if (err) throw err;
      logger.verbose(`Wrote revocation certificate to ${userPath}/revoke.crt`);

      chmod(`${userPath}/revoke.crt`, constants.S_IRUSR | constants.S_IWUSR, chmodErr => {
        if (chmodErr) throw chmodErr;
        logger.verbose(`Changed permissions to -rw------- for ${userPath}/revoke.crt`);
      });
    });
  }

  public static async generatePair({ username, email, passphrase }: IGeneratePair): Promise<GeneratedPair> {
    const now = new Date();
    const address: Address = `sobi-${bs58.encode(Buffer.from(username + now.getTime().toString()))}`;

    const { privateKey, publicKey, revocationCertificate } = await openpgp.generateKey({
      type: "rsa",
      curve: "curve25519",
      userIDs: [{ name: username, email, comment: `Address: ${address}` }],
      passphrase
    });

    return {
      address,
      privateKey,
      publicKey,
      revocationCertificate
    };
  }
  
  /**
   * @param passphrase The passphrase used to decrypt this private key
   */
  public async getPrivateKey(passphrase: string): Promise<openpgp.PrivateKey> {
    const privateKey = await openpgp.decryptKey({
      privateKey: await openpgp.readPrivateKey({ armoredKey: this.#privateKey }),
      passphrase
    });

    return privateKey;
  }

  public async send(to: Address, amount: number, fee = 0, passphrase: string): Promise<Transaction> {
    if (to === this.address) throw new Error("You cannot send SOBI to yourself");

    const newTransaction = await createTransaction({ from: this, to, amount, fee, passphrase });
    return newTransaction;
  }

  public json(): string {

    // TODO: password protect this method

    const obj = {
      comment: "THIS IS A VERY SENSITIVE DOCUMENT. DO NOT LET IT GET OUT OF YOUR HANDS.",
      username: this.username,
      email: this.email,
      address: this.address,
      publicKey: this.publicKey,
      privateKey: this.#privateKey,
      revocationCertificate: this.#revocationCertificate
    };

    return JSON.stringify(obj, null, 2);
  }

}