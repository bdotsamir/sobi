import User from "./User";

interface ICoinbase {
  to: User,
  amount: number
}

export default class Coinbase {

  public readonly to: User;
  public readonly amount: number;

  // internally generated
  public readonly timestamp: Date;

  constructor({ to, amount }: ICoinbase) {
    this.to = to;
    this.amount = amount; 

    this.timestamp = new Date();
  }

  public toString(): string {
    return `COINBASE sent ${this.to} ${this.amount} SOBI on ${this.timestamp}`;
  }
}