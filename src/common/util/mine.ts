import { workerData, parentPort } from "worker_threads";
import { createHash } from "crypto";

import { promisify } from "util";
const wait = promisify(setTimeout);

export default async function mine(): Promise<number> {
  const difficulty = workerData.difficulty;
  const outputInterval = workerData.outputInterval;
  const threads = workerData.threads;
  let nonce = workerData.nonce;
  let found = false;

  // while the hash hasn't been found,
  while (!found) {
    // create a new sha256 hash with the nonce, previous block's hash, and current block's transactions
    const hashed = createHash("sha256").update(JSON.stringify({
      nonce,
      coinbase: workerData.coinbase,
      prevHash: workerData.prevHash,
      transactions: workerData.transactions
    })).digest("hex"); // and convert it into hex

    // post to the main thread the current nonce and hash every (nonce % outputInterval) cycles.
    /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
    if (nonce % outputInterval === 0) parentPort!.postMessage({ nonce, hash: hashed });

    // if the hash doesn't begin with the correct number of zeros, increment the nonce.
    if (!hashed.startsWith(difficulty)) nonce += threads;
    else { // else: yay we found the correct hash!
      // update the main thread one last time before we...
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      parentPort!.postMessage({ nonce, hash: hashed });
      found = true; // ...exit the while loop
    }

    if (workerData.wait)
      await wait(500);
  }

  // and return the nonce just for good measure ;)
  return nonce;
}

mine();