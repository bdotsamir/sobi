import User from "../classes/User";

interface ICreateUser {
  username: string,
  email: string,
  passphrase: string
}

export default async function createUser({ username, email, passphrase }: ICreateUser): Promise<User> {
  const {
    address,
    privateKey: sK,
    publicKey: pK,
    revocationCertificate: rC
  } = await User.generatePair({ username, email, passphrase });

  const newUser = new User({
    address,
    username,
    email,
    privateKey: sK,
    publicKey: pK,
    revocationCertificate: rC
  });

  return newUser;
}