import * as openpgp from "openpgp";
import Transaction from "../classes/Transaction";
import User from "../classes/User";
import { randomBytes } from "crypto";
import promiseHandler from "./promiseHandler";
import Address from "../interfaces/Address";

interface ICreateTransaction {
  from: User,
  to: Address,
  amount: number,
  fee: number,
  passphrase: string
}

export default async function createTransaction({ from, to, amount, fee, passphrase }: ICreateTransaction): Promise<Transaction> {

  const clearText = JSON.stringify({
    fromAddress: from.address,
    toAddress: to,
    amount,
    nonce: randomBytes(8).toString("hex") // just enough randomness
  });

  const [fromPrivKey, error] = await promiseHandler(from.getPrivateKey(passphrase));
  if (error instanceof Error) {
    if (error.message.includes("Incorrect key passphrase")) throw new Error("Incorrect key passphrase");
  }

  const unsignedMessage = await openpgp.createCleartextMessage({ text: clearText });
  const signedMessage = await openpgp.sign({
    message: unsignedMessage,
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    signingKeys: fromPrivKey!
  });

  const newTransaction = new Transaction({ from, to, amount, fee, signature: signedMessage });

  return newTransaction;

}