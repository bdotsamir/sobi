export default interface ReturnCommand {
  success: boolean,
  error?: Error
}