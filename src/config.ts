import { redBright, yellow, greenBright, blueBright, gray } from "chalk";

export default {
  difficulty: "0000",
  blockLength: 5,
  debug: false,
  verbose: false,
  port: +(process.env.PORT ?? 5081),

  prompt: `${redBright("S")}${yellow("O")}${greenBright("B")}${blueBright("I")} ${gray("»")} `
};