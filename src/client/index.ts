process.title = "SOBI Client";

import { readdirSync } from "fs";
import WebSocket from "ws";
import pkg from "../../package.json";
import config from "../config";
import logger from "../common/util/logger";
import ErrorWithCode from "../common/interfaces/ErrorWithCode";
import Address from "../common/interfaces/Address";
import User from "../common/classes/User";

import { program, command } from "bandersnatch";

import checkForInit from "./util/checkForInit";
if (!checkForInit()) {
  logger.error("You must first generate a user for yourself!");
  logger.error("Please run `npm run init`.");
  process.exit(1);
}

import userInformation from "../../user.json";
// TODO: encrypt this information or at least have the option to not have it cached like this.
// this cannot be safe.
export const thisUser = new User({
  address: userInformation.address as Address,
  username: userInformation.username,
  email: userInformation.email,
  privateKey: userInformation.privateKey,
  publicKey: userInformation.publicKey,
  revocationCertificate: userInformation.revocationCertificate,
  dontGenerateFiles: true
});

const URL = process.env.URL ?? "localhost";
export const ws = new WebSocket(`ws://${URL}:${config.port}`);

const app = program()
  .prompt(config.prompt)
  .description(`SOBI Client - v${pkg.version}`);

ws.on("open", init);
ws.on("error", (err: ErrorWithCode) => {
  if (/ECONNREFUSED/gi.test(err.message)) {
    logger.error("Invalid server or server is offline.");
    process.exit(1);
  }

  logger.fatal(err);
  process.exit(err.code ?? 1);
});
ws.on("close", (code: number, reason: Buffer | string) => {
  console.log(" ");
  if (reason && reason instanceof Buffer)
    reason = reason.toString();

  logger.error(`WebSocket closed with code ${code}${reason && `\nReason: ${reason}`}`);
  process.exit(code ?? 1);
});

// TODO: reorder the logic of this so that we load all the commands/events **THEN** connect to ws.
// Here we go!
function init() {
  logger.log(`Connected to ws://${URL}:${config.port}`);

  // Load in all the commands
  const commandFiles = readdirSync("./build/src/client/commands/");
  commandFiles.forEach(async commandFile => {
    const commandName = commandFile.split(".js")[0];
    logger.verbose(`Importing command ${commandName}`);
    const commandModule = await import(`./commands/${commandFile}`);

    // Create a bandersnatch-compatible command with the data we provide in each of the commands
    const bandersnatchCommand = command(commandModule.name)
      .description(commandModule.description)
      .action(commandModule.run);

    // And if the command has arguments, register them here.
    if (commandModule.usage) {
      for (const usage of commandModule.usage) bandersnatchCommand.argument(usage.name, usage.options);
    }

    // Then add the command to the list of commands bandersnatch can use.
    app.add(bandersnatchCommand);
  });
  logger.log("Finished loading commands");

  // I don't care about synchronous issues here because the whole program is starting.
  const events = readdirSync("./build/src/client/events/");
  events.forEach(async eventFile => {
    const eventName = eventFile.split(".js")[0];
    logger.verbose(`Importing websocket event ${eventName}`);
    const event = await import(`./events/${eventFile}`);
    // event.default = the default export for the file
    ws.on(eventName, event.default);
  });
  logger.log("Finished loading events");
  logger.log("Ready for commands");

  // execution will stop here and not continue downward if in REPL mode.
  app.runOrRepl()
    .catch(handleRunOrReplError);
  // prompt();

  if (!app.isRepl()) {
    process.exit();
  }
}

function handleRunOrReplError(reason: unknown) {
  if (reason instanceof Error) {
    if (/not enough non-option arguments/gi.test(reason.message)) {
      return logger.error(reason.message);
    } else if (/did you mean/gi.test(reason.message)) {
      return logger.log(reason.message);
    }
    else return logger.error(reason.message);
  }

  logger.error("Error reason was of type unknown. Outputting to verbose.");
  logger.verbose(reason);
}
