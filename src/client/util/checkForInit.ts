import { accessSync } from "fs";

export default function checkForInit(): boolean {
  try {
    accessSync("./data");
    return true;
  } catch (_) {
    return false;
  }
}