process.title = "SOBI Controller";

import { fork } from "child_process";

console.log("Starting server...");
const server = fork("./build/src/server/index.js");
server.on("spawn", () => console.log("Server successfully forked"));

console.log("Starting client...");
const client = fork("./build/src/client/index.js");
client.on("spawn", () => console.log("Client successfully forked"));

client.on("close", (code, signal) => {
  console.log("Client closed. Stopping server...");
  console.log("Code:", code, "Signal:", signal);

  server.kill();

  console.log("Server closed. Exiting gracefully...");
  process.exit(0);
});
