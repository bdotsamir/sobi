// This is meant to be a temporary file while I develop the rest of the server.
// Feel free to copy code from this file to the actual miner files but this is
// by no means the final product.

console.to   = (...args) => console.log('[CLIENT => WS]', ...args);
console.from = (...args) => console.log('[WS => CLIENT]', ...args);

const WebSocket = require('ws');
require('dotenv').config({ path: '../.env' });

const port = process.env.PORT || 42069;
const ws = new WebSocket(`ws://localhost:${port}`);

ws.on('open', () => console.log('Connected!'));
ws.on('close', (code, reason) => {
  if (code === 1006) reason = 'Broken connection';
  console.from(`Connection closed\nCode ${code} | Reason: ${reason}`);
});

ws.on('message', message => {
  // The data will be a string when it comes through. Parse it into an object.
  message = JSON.parse(message);

  console.from(message);

  switch (message.action) {
    case 'identify': {
      ws.send(JSON.stringify({
        action: 'identify',
        key: process.env.WEBSOCKET_KEY,
        type: 'miner'
      }));

      console.to('Sent identify packet...');
      break;
    }
    case 'identify_OK': {
      console.from('Identified. Listening for transactions...');
      break;
    }

    case 'transaction': {
      console.from('NEW TRANSACTION');
      break;
    }

    case 'found': {
      console.from(`BLOCK FOUND: ${message.hash}`);
      console.log('damn, someone found it before me >:(');
    }
  }
});

ws.on('error', error => {
  if (error.code === 'ECONNREFUSED') return console.error('Connection refusued. Is the server online?');
  else console.error(error);
});

function mine() {

}